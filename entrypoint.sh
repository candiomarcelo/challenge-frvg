#!/bin/bash

set -e
#export PATH=$PATH:/opt/conda/envs/default/bin/jupyter
JUPYTER="/opt/conda/envs/default/bin/jupyter"
IP="--ip=0.0.0.0"
PORT="--port=8888"
ROOT="--allow-root"
BROWSER="--no-browser"
echo 'Running jupyter lab'  
if [ "$1" = 'jupyter' ]; then
    exec $JUPYTER lab $IP $PORT $ROOT $BROWSER

fi

#"jupyter lab" $IP $PORT $BROWSER $ROOT  