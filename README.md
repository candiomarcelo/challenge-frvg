# jupyter-data

Infraestructura basada en AWS con instancia ubuntu 

Herramienta para analizar datos basado en jupyter-lab

default pass at ${INSTANCE_IP}:8888 is vida1234

El proyecto cuenta con gitlab-runner instalado sobre una instancia de AWS donde a su vez utiliza Docker como herramienta de contenerizacion y docker-compose como herramienta de orquestación de servicio/contenedor

El proyecto tiene dos escenarios via Gitlab-CI
 1 - build de una imagen docker guardandola en el registry del proyecto y poniendo tag del commit (short-tag) 
 2 - Deploy de la imagen con el tag específico sobre una instancia de AWS

La instancia de AWS permite el acceso externo solo a las ips especificadas en un security group


