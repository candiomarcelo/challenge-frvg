FROM continuumio/miniconda3
RUN apt-get  update && apt-get upgrade -y && apt-get -y install apt-utils gcc

ARG ENVIRONMENT
EXPOSE 8888

RUN echo "source activate env" > ~/.bashrc
ENV PATH /opt/conda/envs/env/bin:$PATH
ADD /Requirements/requirements.yaml /tmp/requirements.yaml
# Pull the requirements name out of the requirements.yml
RUN conda env create -f /tmp/requirements.yaml
#RUN conda  create -n mcand 
RUN echo "source activate $(head -1 /tmp/requirements.yaml | cut -d' ' -f2)" > ~/.bashrc
ENV PATH /opt/conda/envs/$(head -1 /tmp/requirements.yaml | cut -d' ' -f2)/bin:$PATH
RUN mkdir /etc/notebooks
WORKDIR /etc/notebooks
ADD entrypoint.sh /tmp/entrypoint.sh
RUN mkdir /root/.jupyter/
#ADD /Requirements/jupyter_notebook_config.py /root/.jupyter/jupyter_notebook_config.py
ADD /Requirements/jupyter_notebook_config.json /root/.jupyter/jupyter_notebook_config.json
RUN chmod +x  /tmp/entrypoint.sh 
ENV PATH /opt/conda/envs/default/bin:$PATH
ENTRYPOINT [ "/bin/sh","-e","/tmp/entrypoint.sh", "jupyter" ]
CMD ["jupyter", "lab"] 